
       Rainbow Disk Image Utilities
       ============================

       The files contained in this archive are utilities for creating disk
   images from RX50s and regenerating disks from said images.  The
   utilities are meant to work with MS-DOS version 2.11 or later on an
   actual Rainbow 100 personal computer only.

       The programs utilize RAW disk image format, which is a byte-by-byte
   dump of all data on the disk in increasing track and sector order.  The
   RX50 disk drive (at least as implemented on the Rainbow 100) is an
   80-track drive with 10 sectors/track and 512 bytes/sector.  The RAW
   image format for a Rainbow would be as follows:

       Beginning of file: Track 0 Sector 1
                          Track 0 Sector 2
                          ...
                          Track 0 Sector 10
                          Track 1 Sector 0
                          ...
       End of file:       Track 79 Sector 10

       With each sector containing 512 bytes, a valid RX50 image should be
   exactly 409,600 bytes long.  If the image file is a different size when
   recreating a floppy disk, results are unpredictable.

       These utilities make use of Rainbow interrup 0x65 for all
   reading/writing.  A true Rainbow RX50 floppy has sector skewing
   implemented in both MSDOS and CP/M, meaning that physical sectors do not
   equal logical sectors.  Logical sectors are the way the operating
   system(s) access the floppies, whereas the physical sectors represent
   the actual layout on the disk.  The call to interrupt 0x65 assumes the
   calling program wishes to access everything by logical sector.  The RAW
   image format, however, would actually use physical sectors.  The
   utilities automagically "unskew" the sectors when creating an image and
   "reskew" the sectors when writing the disk.

       Logically addressed images can be dealt with as well by using the /L
   option with the utilities.  This option enables logical addressing.  The
   benefit of a logically addressed image file is that it may (but not for
   sure...) be able to be mounted under operating systems that allow
   mounting image files (i.e. GNU/Linux or *BSD).

       A summary of the utilities appears below:

       iwrite.exe
       ==========

       The iwrite utility is used for regenerating a floppy disk from an
   image file.  The syntax is:

       iwrite F:\path\to\image.img Drive

       The Drive should be a single letter specifying an RX50 drive to
   write to (A, B, C, or D, where applicable).  Formatting the floppies
   prior to writing should not be necessary.  For example, to write the
   image file "dos310b.img" to the B: drive, the following command would be
   used:

       iwrite dos310b.img B

       The /L option can be used if the image file is known to be logically
   addressed.

       icreate.exe
       =========

       The icreate utility will create a new image file by reading the
   contents of a true RX50 diskette.  The syntax is identical to the iwrite
   utility:

       icreate F:\path\to\image.img Drive

       The Drive parameter is a single letter specifying the drive
   containing the disk to image (A, B, C, or D, where applicable).  The
   drive must be a true RX50 drive.  For example, to image a disk in the A:
   drive and store the result in the file "cpmv20.img", the following
   command line would be used:

       icreate cpmv20.img A

       The /L option can be used to generate a logically addressed image if
   desired.

       Skewing
       =======

       As stated ealier, the Rainbow employs a skewing scheme on RX50
   diskettes.  Tracks 0 and 1 are not skewed (logical sector == physical
   sector), whereas all other tracks use the following skew table:

       Physical    Logical
       1           1
       2           6
       3           2
       4           7
       5           3
       6           8
       7           4
       8           9
       9           5
       10          10

       I do not personally know the reasons for the above, but the table
   works.  The table was lifted from the source code of ImpDrive by M.
   Warner Losh.

       Compatibility
       =============

       These utilities use a true RAW image format.  The iwrite utility has
   been tested with and is completely compatible with Will Kranz's
   wteledisk utility.  Wteledisk can convert Teledisk *.td0 images into RAW
   image files.  The resulting RAW image file can then be written to an
   RX50 using the iwrite utility.  The /L option should not be used in
   these cases.

       Building
       ========

       The utilities were compiled using Borland Turbo C 2.0 on an actual
   Rainbow 100.  The executables can be generating by running:

       make all

       The code does not use any Borland extensions, but does rely on the
   int86() and segread() functions from dos.h.  It should be possible to
   compile this code with Microsoft C as well, although the make file
   certainly will not work.

       Support
       =======

       Anyone needing support with this utility or encountering bugs with
   various images can contact me at jeff@rainbow-100.com.  These utilities,
   although at version 0.1, are completely capable.

       Copyright
       =========

       The utilities and their source code are copyrighted by Jeff
   Armstrong and released under the GNU General Public License version 3.

       The real meat of the licensing:

       Rainbow Image Utilities
       Copyright � 2008 Jeffrey Armstrong <jeff@rainbow-100.com>
       http://jeff.rainbow-100.com/

       This program is free software: you can redistribute it and/or modify
       it under the terms of the GNU General Public License as published by
       the Free Software Foundation, either version 3 of the License, or
       (at your option) any later version.
       
       This program is distributed in the hope that it will be useful
       but WITHOUT ANY WARRANTY; without even the implied warranty of
       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
       GNU General Public License for more details.
       
       You should have received a copy of the GNU General Public License
       along with this program.  If not, see <http://www.gnu.org/licenses/>.
       
       The full license is compressed in COPYING.ZIP.

       ---
       Jeff Armstrong
       http://jeff.rainbow-100.com/
       jeff@rainbow-100.com

       January 15, 2009
