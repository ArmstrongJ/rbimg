CC=tcc

rbfloppy.obj: rbfloppy.c rbfloppy.h
        $(CC) -c rbfloppy.c
        
util.obj: util.c util.h
        $(CC) -c util.c
        
iwrite.exe: iwrite.c rbfloppy.obj util.obj
        $(CC) iwrite.c util.obj rbfloppy.obj
        
icreate.exe: icreate.c rbfloppy.obj util.obj
        $(CC) icreate.c rbfloppy.obj util.obj
        
all: icreate.exe iwrite.exe

clean:
        del *.obj
        del iwrite.exe
        del icreate.exe
        