/*       Rainbow Image Utilities
 *       Copyright � 2008 Jeffrey Armstrong <jeff@rainbow-100.com>
 *       http://jeff.rainbow-100.com/
 *
 *       This program is free software: you can redistribute it and/or modify
 *       it under the terms of the GNU General Public License as published by
 *       the Free Software Foundation, either version 3 of the License, or
 *       (at your option) any later version.
 *       
 *       This program is distributed in the hope that it will be useful
 *       but WITHOUT ANY WARRANTY; without even the implied warranty of
 *       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *       GNU General Public License for more details.
 *       
 *       You should have received a copy of the GNU General Public License
 *       along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rbfloppy.h"

/* Small utility to check if the command line arguments contain a /L
 * switch.  If so, the arguments are all shifted so that the /L is
 * effectively removed from the list
 */
void check_logical(int argc, char *argv[])
{
int i,j;

        for(i=1; i<argc; i++) {
                if(strlen(argv[i]) == 2) {

                        /* This is a really ugly, long-winded way to
                         * check for the switch... 
                         */
                        if((argv[i][0] == '/' || argv[i][0] == '-') &&
                           (argv[i][1] == 'L' || argv[i][1] == 'l')) {
                            set_mode(MODE_LOGICAL);
                            break;
                        }
                }
        }
        
        /* If the argument was found, shift everyone to "remove" it */
        if(i < argc && i != argc-1) {
                for(j=i;j<argc-1;j++) {
                        argv[j] = argv[j+1];
                }
        }
}
