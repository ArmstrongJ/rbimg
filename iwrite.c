/*       Rainbow Image Utilities
 *       Copyright � 2008 Jeffrey Armstrong <jeff@rainbow-100.com>
 *       http://jeff.rainbow-100.com/
 *
 *       This program is free software: you can redistribute it and/or modify
 *       it under the terms of the GNU General Public License as published by
 *       the Free Software Foundation, either version 3 of the License, or
 *       (at your option) any later version.
 *       
 *       This program is distributed in the hope that it will be useful
 *       but WITHOUT ANY WARRANTY; without even the implied warranty of
 *       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *       GNU General Public License for more details.
 *       
 *       You should have received a copy of the GNU General Public License
 *       along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/* iwrite - A utility for creating a floppy from an image
 * by Jeff Armstrong
 */

#include <stdio.h>

#include "rbfloppy.h"
#include "util.h"

/* Function which fills the passed buffer with 512 bytes from the file
 * representing the next sector of data for writing out.
 */
int get_next_sector(FILE *fp, char *buffer)
{
int readin;
	if(fp == NULL)
		return ERROR;

        for(readin=0;readin<BYTES_PER_SECTOR && feof(fp)==0;readin++)
                buffer[readin] = (char)fgetc(fp);
	
        /*readin = fread(buffer,sizeof(char),BYTES_PER_SECTOR,fp); */

	if(readin == BYTES_PER_SECTOR)
		return OK;
	else
		return ERROR;
}

int main(int argc, char *argv[])
{
int drv, sector, track;
char *tmp;
FILE *fp;

char *data;

int image_errors, write_errors;
        
        printf("Rainbow Image Writer 0.1\nCopyright %c 2008 Jeff Armstrong\n\n",0xA9);
        
        if(argc < 3) {
                printf("Usage: %s [/L] <image file> <target drive>\n");
                printf("       Target drive should be one of: A, B, C, D\n");
                printf("\nOptions:\n");
                printf("       /L = image contains logical sectors\n");
                return 0;
        }
        
        /* The mode is assumed to be physical addressing, but we'll check */
        set_mode(MODE_PHYSICAL);
        check_logical(argc, argv);
        
        printf("Writing image using ");
        if(get_mode() == MODE_LOGICAL)
                printf("logical ");
        else
                printf("physical ");
        printf("sectors.\n");

        /* Open the image file for reading */
        fp = fopen(argv[1],"rb");
        if(fp == NULL) {
                printf("Could not open image file:\n\t%s\n",argv[1]);
                return 0;
        }

        /* Determine drive */
        tmp = argv[2];
        drv = drive_id(tmp[0]);
        if(drv < 0) {
                printf("Could not process drive request %s\n",tmp);
                return 0;
        } 

        /* Allocate a data buffer that will hold exactly one sector of data */
        data = (char *)malloc(BYTES_PER_SECTOR*sizeof(char));

        printf("\nWriting %s to floppy...\n",argv[1]);
        
        /* Initialize error counters */
        image_errors = 0; write_errors = 0;
        
        /* Cycle through all tracks from 0 to 79 */
        for(track=0;track<TRACK_MAX;track++) {
        
                /* Note that sector count is 1-based */
                for(sector=1;sector<=SECTOR_MAX;sector++) {
                
                        printf("\rTrack: %d Sector: %d  ",track,sector);
                        if(get_next_sector(fp,data) != OK)
                                image_errors++;
                        else {
                                if(write_sector(drv,track,sector,data) != OK)
                                        write_errors++;
                        }
                }
                /* sleep(5); */
        }

        if(image_errors == 1)
                printf("\n%c1 image error encountered during read.",007);
        else if(image_errors > 1)
                printf("\n%c%d image errors encountered during read.",007,image_errors);
        
        if(write_errors == 1)
                printf("\n%c1 write error encountered during drive access.",007);
        else if(write_errors > 1)
                printf("\n%c%d write errors encountered during drive acces.",007,write_errors);

        printf ("\n\nProcessing complete.");
        
        fclose(fp);
        
        free(data);

        return 0;
}