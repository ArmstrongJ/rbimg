/*       Rainbow Image Utilities
 *       Copyright � 2008 Jeffrey Armstrong <jeff@rainbow-100.com>
 *       http://jeff.rainbow-100.com/
 *
 *       This program is free software: you can redistribute it and/or modify
 *       it under the terms of the GNU General Public License as published by
 *       the Free Software Foundation, either version 3 of the License, or
 *       (at your option) any later version.
 *       
 *       This program is distributed in the hope that it will be useful
 *       but WITHOUT ANY WARRANTY; without even the implied warranty of
 *       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *       GNU General Public License for more details.
 *       
 *       You should have received a copy of the GNU General Public License
 *       along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* icreate - A utility for creating a disk image file from an RX50
 * by Jeff Armstrong
 */

#include <stdio.h>
#include "rbfloppy.h"
#include "util.h"

/* This method will return a blank (all \0) data buffer of 512 bytes
 * for use when reading a specific sector fails.
 */
void blank_buffer(char *buffer, int length)
{
int i;

        for(i=0;i<length;i++)
                buffer[i] = (char)0;
}

int main(int argc, char *argv[])
{
int drv, sector, track;
char *tmp;
FILE *fp;
int i;

char *data;
int floppy_errors;
        
        printf("Rainbow Image Creator 0.1\nCopyright %c 2008 Jeff Armstrong\n\n",0xA9);
        
        if(argc < 3) {
                printf("Usage: %s <image file> <source drive>\n");
                printf("       Source drive should be one of: A, B, C, D\n");
                printf("\nOptions:\n");
                printf("       /L = create image with logical sectors\n");
                return 0;
        }

        /* Determine if logical addressing was requested */
        set_mode(MODE_PHYSICAL);
        check_logical(argc, argv);
        
        printf("Creatting image using ");
        if(get_mode() == MODE_LOGICAL)
                printf("logical ");
        else
                printf("physical ");
        printf("sectors.\n");
        
        /* Open the target file to receive the disk image */
        fp = fopen(argv[1],"wb");
        if(fp == NULL) {
                printf("Could not open image file:\n\t%s\n",argv[1]);
                return 0;
        }

        /* Determine drive */
        tmp = argv[2];
        drv = drive_id(tmp[0]);
        if(drv < 0) {
                printf("Could not process drive request %s\n",tmp);
                return 0;
        } 

        /* Allocate a 512 byte buffer */
        data = (char *)malloc(BYTES_PER_SECTOR*sizeof(char));

        printf("\nWriting floppy to %s ...\n",argv[1]);
        floppy_errors = 0;

        for(track=0;track<TRACK_MAX;track++) {
        
                /* Note that sector count is 1-based */
                for(sector=1;sector<=SECTOR_MAX;sector++) {

                        printf("\rTrack: %d Sector: %d  ",track,sector);
                        
                        if(read_sector(drv,track,sector,data) != OK) {
                                blank_buffer(data,BYTES_PER_SECTOR);
                                floppy_errors++;
                        } 
                        for(i=0;i<BYTES_PER_SECTOR;i++)
                                fputc(data[i],fp);
                
                }
                /* sleep(5); */
        }

        if(floppy_errors == 1)
                printf("\n%c1 floppy error encountered during read.",007);
        else if(floppy_errors > 1)
                printf("\n%c%d floppy errors encountered during read.",007,floppy_errors);

        printf ("\n\nProcessing complete.");
        
        fclose(fp);
        
        free(data);

        return 0;
}
