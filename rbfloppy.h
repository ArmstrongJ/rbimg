/*       Rainbow Image Utilities
 *       Copyright � 2008 Jeffrey Armstrong <jeff@rainbow-100.com>
 *       http://jeff.rainbow-100.com/
 *
 *       This program is free software: you can redistribute it and/or modify
 *       it under the terms of the GNU General Public License as published by
 *       the Free Software Foundation, either version 3 of the License, or
 *       (at your option) any later version.
 *       
 *       This program is distributed in the hope that it will be useful
 *       but WITHOUT ANY WARRANTY; without even the implied warranty of
 *       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *       GNU General Public License for more details.
 *       
 *       You should have received a copy of the GNU General Public License
 *       along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __RAINBOW_FLOPPY_HEADERS
#define __RAINBOW_FLOPPY_HEADERS

/* Commands understood by access_sector() */
#define CMD_WRITE               1
#define CMD_WRITE_VERIFY        2
#define CMD_READ                0

/* The floppy access interrupt */
#define FLOPPY_INTR             0x65

/* The track and sector counts */
#define TRACK_MAX       80
#define SECTOR_MAX      10

/* Status codes */
#define OK	0
#define ERROR	-1
#define ERROR_BAD_SECTOR        -2
#define ERROR_FROM_BIOS         -3

/* The bytes per sector on an RX50 */
#define BYTES_PER_SECTOR	512

/* The sector addressing mode flags */
#define MODE_PHYSICAL   1
#define MODE_LOGICAL    2

/* Writes a given sector using the data in buffer */
int write_sector(int unit, int track, int sector, char *buffer);

/* Reads a specified sector from disk into buffer */
int read_sector(int unit, int track, int sector, char *buffer);

/* Internal routine that performs all actual disk access through calls
 * to interrupt 0x65.  Some command ints are already #defined, but 
 * a few more exist that aren't listed here.
 */
int access_sector(int command, int unit, int track, int sector, char *buffer);

/* Determines the unit number for access_sector() from a given character.
 * Expects a drive letter or a number.  Returns -1 on error.
 */
int drive_id(char id);

/* Sets the mode of the sector addressing to either physical or
 * logical by assigning a static variable.
 */
void set_mode(int mode);

/* Returns the current addressing mode */
int get_mode();

#endif /* __RAINBOW_FLOPPY_HEADERS */
